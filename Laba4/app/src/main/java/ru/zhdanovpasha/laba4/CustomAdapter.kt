package ru.zhdanovpasha.laba4

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.support.v4.content.ContextCompat.startActivity
import android.content.Intent
import android.net.Uri
import java.net.URI


/**
 * Created by ZhdanovPasha on 09.03.2018.
 */
class CustomAdapter(val planetList: ArrayList<Planet>): RecyclerView.Adapter<CustomAdapter.ViewHolder>() {
    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.nameText?.text = planetList[position].name
        holder?.image?.setImageBitmap(planetList[position].image)
        holder?.distanceText?.text = planetList[position].distance
        holder?.isLiveImage?.setImageBitmap(planetList[position].liveIcon)
        holder?.linkText?.text = planetList[position].link
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent?.context).inflate(R.layout.item, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return planetList.size
    }

    class ViewHolder(planetView: View): RecyclerView.ViewHolder(planetView) {
        val nameText = planetView.findViewById<TextView>(R.id.name_text)
        val image = planetView.findViewById<ImageView>(R.id.img)
        val distanceText = planetView.findViewById<TextView>(R.id.distance_text)
        val isLiveImage = planetView.findViewById<ImageView>(R.id.live_image)
        val linkText = planetView.findViewById<TextView>(R.id.link_text)

        init {
            planetView.setOnClickListener {
                val intent = Intent(planetView.context, NetActivity::class.java)
                intent.putExtra("link", linkText.text.toString())
                planetView.context.startActivity(intent)
            }
        }
    }
}