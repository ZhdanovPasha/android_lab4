package ru.zhdanovpasha.laba4

import android.graphics.Bitmap

/**
 * Created by ZhdanovPasha on 09.03.2018.
 */
data class Planet(val name:String, val image:Bitmap, val distance: String, val liveIcon: Bitmap, val link: String)