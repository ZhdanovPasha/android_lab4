package ru.zhdanovpasha.laba4

import android.graphics.BitmapFactory
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.LinearLayout

class MainActivity : AppCompatActivity() {

    lateinit var planets: ArrayList<Planet>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()

        val rv = findViewById<RecyclerView>(R.id.recycler_view)
        rv.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)

        planets = ArrayList<Planet>()
        initPlanets()

        val adapter = CustomAdapter(planets)
        rv.adapter = adapter
    }

    private fun initPlanets(){
        planets.add(Planet("Mercury", BitmapFactory.decodeResource(resources, R.drawable.mercury),"58 000 000", BitmapFactory.decodeResource(resources, R.drawable.is_not_live), "https://en.wikipedia.org/wiki/Mercury_(planet)"))
        planets.add(Planet("Venus", BitmapFactory.decodeResource(resources, R.drawable.venera),"108 200 000", BitmapFactory.decodeResource(resources, R.drawable.is_not_live), "https://en.wikipedia.org/wiki/Venus"))
        planets.add(Planet("Earth", BitmapFactory.decodeResource(resources, R.drawable.earth),"149 600 000", BitmapFactory.decodeResource(resources, R.drawable.is_live), "https://en.wikipedia.org/wiki/Earth"))
        planets.add(Planet("Mars", BitmapFactory.decodeResource(resources, R.drawable.mars),"227 900 000", BitmapFactory.decodeResource(resources, R.drawable.is_not_live), "https://en.wikipedia.org/wiki/Mars"))
        planets.add(Planet("Jupiter", BitmapFactory.decodeResource(resources, R.drawable.jupiter),"778 000 000", BitmapFactory.decodeResource(resources, R.drawable.is_not_live), "https://en.wikipedia.org/wiki/Jupiter"))
        planets.add(Planet("Saturn", BitmapFactory.decodeResource(resources, R.drawable.saturn),"1 427 000 000", BitmapFactory.decodeResource(resources, R.drawable.is_not_live), "https://en.wikipedia.org/wiki/Saturn"))
        planets.add(Planet("Uranus", BitmapFactory.decodeResource(resources, R.drawable.uran),"2 871 000 000", BitmapFactory.decodeResource(resources, R.drawable.is_not_live), "https://en.wikipedia.org/wiki/Uranus"))
        planets.add(Planet("Neptune", BitmapFactory.decodeResource(resources, R.drawable.neptun),"4 497 000 000", BitmapFactory.decodeResource(resources, R.drawable.is_not_live), "https://en.wikipedia.org/wiki/Neptune"))
    }

}
